extern crate sfml;

pub mod entity;
pub mod screen;
pub mod gameresource;

use screen::Screen;
use gameresource::GameResource;


fn main() {
    println!("DODHEIM");
    let mut resources = GameResource::new();
    let mut screen = Screen::new(1024, 768);
    resources.load_texture("resources/grass_1flow.png");

    let texture = resources.get_texture();
    screen.create_entities(texture);
    screen.game_loop();
}
