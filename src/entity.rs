//! 
//! The sprite wrapper module
//! 
extern crate sfml;

use std::vec;
use self::sfml::system::Vector2;
use self::sfml::graphics::{Transformable, Texture, Sprite, IntRect};


struct Animation {
    frames: vec::Vec<i32>,
    i: i32,
}


impl<'t> Animation {
    pub fn new() -> Self {
        let i = -1;
        let frames = vec::Vec::new();
        Animation {
            frames: frames,
            i: i,
        }
    }

    pub fn push(&mut self, i: i32) {
        self.frames.push(i);
    }

    pub fn next_frame(&mut self) -> i32 {
        self.i += 1;
        if self.i >= self.frames.len() as i32 {
            self.i = 0;
        }
        self.frames[self.i as usize]
    }

    pub fn next_rect(&mut self) -> IntRect {
        IntRect::new(self.next_frame(), 0, 32, 32)
    }
}


pub struct Entity<'t> {
    pub sprite: Sprite<'t>,
    animation: Animation,
}


impl<'t> Entity<'t> {
    pub fn new(texture: &'t Texture, x: f32, y: f32) -> Self {
        let mut animation = Animation::new();
        animation.push(0);
        animation.push(32);
        let mut sprite = Sprite::new();
        let pos = Vector2::new(x, y);
        sprite.set_texture(texture, false);
        sprite.set_position(&pos);
        Entity {
            sprite: sprite,
            animation: animation,
        }
    }

    pub fn set_rect(&mut self, x: i32, y: i32) {
        self.sprite.set_texture_rect(&IntRect::new(x, y, 32, 32));
    }

    pub fn animate(&mut self) {
        self.sprite.set_texture_rect(&self.animation.next_rect());
    }
}


