extern crate sfml;

use std::vec;

use sfml::graphics::Texture;


pub struct GameResource {
    textures: vec::Vec<Texture>,
}


impl GameResource {
    pub fn new() -> Self {
        GameResource {
            textures: vec::Vec::new(),
        }
    }
    
    pub fn load_texture(&mut self, path: &str) {
        let texture = Texture::from_file(path).unwrap();
        self.textures.push(texture);
    }

    pub fn get_texture(&mut self) -> &Texture {
        &self.textures[0]
    }
}
